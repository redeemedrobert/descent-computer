#include "descentcomputer.h"
#include <cmath>

DescentComputer::DescentComputer(QObject *parent)
    : QObject{parent}
{
    debounce = new QTimer(this);
    debounce->setSingleShot(true);
    debounce->setInterval(1000);
    connect(debounce, &QTimer::timeout, this, &DescentComputer::gridWork);
}

void DescentComputer::calculateGrid() {

    if(debounce->isActive())
        return;
    debounce->start();
}

void DescentComputer::gridWork() {
    m_grid_contents.clear();

    if(!m_flipped) {
        for(qint16 i = 0; i < m_rows; ++i) {

            for(qint16 j = 0; j < m_columns; ++j) {

                if(j == 0 && i == 0) { // Grid position 1,1
                    m_grid_contents.append("nmi >\nv kt");
                    continue;
                } else if(i == 0) { // Grid position 1,X; show distances
                    m_grid_contents.append(QString::number(m_start_dist + m_dist_step * (j-1)));
                    continue;
                } else if(j == 0) { // Grid position X,1; show speeds
                    m_grid_contents.append(QString::number(m_start_speed + m_speed_step * (i-1)));
                    continue;
                } else {
                     double speed = m_start_speed + m_speed_step * (i-1);
                     double milesPerMinute = speed / 60; // nautical miles per minute
                     double dist = m_start_dist + m_dist_step * (j-1);
                     double time = dist / milesPerMinute;
                     qint16 mins = time;
                     qint16 secs = round((time - floor(time)) * 60);
                     QString out = time == 0 ? "N/A" : QString::number(mins) + ":"
                                               + (secs < 10 ? "0" + QString::number(secs) : QString::number(secs));
                     m_grid_contents.append(out);
                }
            }
        }
    } else {
        for(qint16 i = 0; i < m_rows; ++i) {

            for(qint16 j = 0; j < m_columns; ++j) {

                double time = m_start_time + m_time_step * (j-1);
                if(j == 0 && i == 0) { // Grid position 1,1
                    m_grid_contents.append("min >\nv alt");
                    continue;
                } else if(i == 0) { // Grid position 1,X; show times
                    if(time < 0) time = 0; // no point in showing negatives
                    qint16 mins = time;
                    qint16 secs = round((time - floor(time)) * 60);
                    QString out = time == 0 ? "N/A" : QString::number(mins) + ":"
                                              + (secs < 10 ? "0" + QString::number(secs) : QString::number(secs));
                    m_grid_contents.append(out);
                    continue;
                } else if(j == 0) { // Grid position X,1; show altitudes
                    m_grid_contents.append(QString::number(m_start_alt + m_alt_step * (i-1)));
                    continue;
                } else {
                     double time = m_start_time+ m_time_step * (j-1);
                     qint32 alt = m_start_alt + m_alt_step * (i-1);
                     QString out = QString::number(alt/time);
                     if(time < 0) out = "N/A";
                     m_grid_contents.append(out.indexOf(".") > -1 ? out.left(out.indexOf(".")) : out); // floor float
                }
            }
        }
    }
    emit gridContentsChanged();
}

void DescentComputer::setCursor(qint16 x, qint16 y) {

    m_cursor_x = x; m_cursor_y = y;
    double speed = m_start_speed + m_speed_step * (y-1);
    double milesPerMinute = speed / 60;
    double dist = m_start_dist + m_dist_step * (x-1);
    double time = dist / milesPerMinute;
    m_start_time = time - m_time_step * (x-1);
}

void DescentComputer::setTimeStep(const double val) { // have to also recalculate timeStart so that the current time column stays the same
    m_time_step = val;
    double speed = m_start_speed + m_speed_step * (m_cursor_y-1);
    double milesPerMinute = speed / 60;
    double dist = m_start_dist + m_dist_step * (m_cursor_x-1);
    double time = dist / milesPerMinute;
    m_start_time = time - m_time_step * (m_cursor_x-1);
    emit timeStepChanged();
    calculateGrid();
}
