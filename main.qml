import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material

Window {
    property int cellW: 60 // width of cell
    property int cellH: 45 // height of cell

    id: window
    Material.theme: Material.Light
    Material.accent: Material.Grey
    width: cellW * 11 // fixed cell width * 11 // old default: 640
    height: cellH * 11 + titlebar.height // fixed cell height * 11 // old default: 480
    visible: true
    title: qsTr("DC1 Descent Computer")
    flags: Qt.FramelessWindowHint | Qt.MSWindowsFixedSizeDialogHint

    onWidthChanged: {
        contentgrid.opacity = 0
        var columns = Math.floor(width/cellW)
        dc.columns = columns < 2 ? 2 : columns
        width = dc.columns * cellW
    }
    onHeightChanged: {
        contentgrid.opacity = 0
        var rows = Math.floor((height - titlebar.height) / cellH)
        dc.rows = rows < 2 ? 2 : rows
        height = dc.rows * cellH + titlebar.height
    }

    ColumnLayout {
        anchors.fill: parent
        spacing: 0
        TitleBar {
            id: titlebar
        }

        CompGrid {
            id: contentgrid
            Layout.fillHeight: true
            Layout.fillWidth: true
            Behavior on opacity {
                NumberAnimation {
                    easing.type: Easing.OutCirc
                    duration: 500
                }
            }
            Connections {
                target: dc
                function onGridContentsChanged() {
                    contentgrid.opacity = 1
                }
            }
        }
    }

    SettingsPanel {
        id: settingspanel
        height: window.height - titlebar.height
        width: window.width
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        opacity: 0
        visible: opacity > 0
        Behavior on opacity {
            NumberAnimation {
                easing.type: Easing.OutCirc
                duration: 500
            }
        }
    }

    Text {
        text: "◢"
        x: window.width - 20
        y: window.height - 20
        font.pixelSize: 20
        visible: Qt.platform.os === "windows" || Qt.platform.os === "osx" || Qt.platform.os === "linux"
        MouseArea {
            property int offsetX
            property int offsetY
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.SizeFDiagCursor
            onPressed: {
                offsetX = (x + width / 2) - mouseX
                offsetY = (y + height / 2) - mouseY
                wm.startSizeTracking()
            }

            onPositionChanged: (mouse) => {
                if(pressed) {
                   window.width += mouseX - (x + width / 2) + offsetX
                   window.height += mouseY - (y + height / 2) + offsetY
               }
            }
        }
    }
}
