import QtQuick
import QtQuick.Layouts
import QtQuick.Controls
import QtQuick.Controls.Material

Rectangle {
    color: "#90000000"
    Material.theme: Material.Light
    Material.accent: Material.Grey
    // layer.enabled: true
    Rectangle {
        anchors.centerIn: parent
        width: parent.width * .75
        height: parent.height * .8
        color: "#F0F0F0"
        clip: true
        ColumnLayout {
            anchors.fill: parent
            spacing: 5
            Text {
                Layout.topMargin: 5
                Layout.fillWidth: true
                text: "Starting G/S(kt)"
                horizontalAlignment: Text.AlignHCenter
                visible: !dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 1500
                stepSize: 10
                Component.onCompleted: value = dc.startSpeed
                // not initializing the value property with the property binding to avoid a bind loop
                // only setting value once onCompleted instead
                onValueChanged: dc.startSpeed = value
                visible: !dc.flipped
            }
            MenuSeparator {
                Layout.fillWidth: true
                visible: !dc.flipped
            }
            Text {
                Layout.fillWidth: true
                text: "Step size(kt):"
                horizontalAlignment: Text.AlignHCenter
                visible: !dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 500
                stepSize: 10
                Component.onCompleted: value = dc.speedStep
                onValueChanged: dc.speedStep = value
                visible: !dc.flipped
            }
            MenuSeparator {
                Layout.fillWidth: true
                visible: !dc.flipped
            }
            Text {
                text: "Starting distance(nmi):"
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                visible: !dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 1500
                stepSize: 10
                Component.onCompleted: value = dc.startDist
                onValueChanged: dc.startDist = value
                visible: !dc.flipped
            }
            MenuSeparator {
                Layout.fillWidth: true
                visible: !dc.flipped
            }
            Text {
                Layout.fillWidth: true
                text: "Step size(nmi):"
                horizontalAlignment: Text.AlignHCenter
                visible: !dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 500
                stepSize: 10
                Component.onCompleted: value = dc.distStep
                onValueChanged: dc.distStep = value
                visible: !dc.flipped
            }
            Text {
                text: "Starting altitude(ft):"
                Layout.topMargin: 5
                Layout.fillWidth: true
                horizontalAlignment: Text.AlignHCenter
                visible: dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 100000
                stepSize: 100
                Component.onCompleted: value = dc.startAlt
                onValueChanged: dc.startAlt = value
                visible: dc.flipped
            }
            MenuSeparator {
                Layout.fillWidth: true
                visible: dc.flipped
            }
            Text {
                Layout.fillWidth: true
                text: "Step size(ft):"
                horizontalAlignment: Text.AlignHCenter
                visible: dc.flipped
            }
            SpinBox {
                editable: true
                Layout.fillWidth: true
                from: 0
                to: 10000
                stepSize: 100
                Component.onCompleted: value = dc.altStep
                onValueChanged: dc.altStep = value
                visible: dc.flipped
            }
            MenuSeparator {
                Layout.fillWidth: true
                visible: dc.flipped
            }
            Text {
                Layout.fillWidth: true
                text: "Time step size(mm:ss):"
                horizontalAlignment: Text.AlignHCenter
                visible: dc.flipped
            }
            Row {
                Layout.alignment: Qt.AlignHCenter
                visible: dc.flipped
                SpinBox {
                    id: timestepmins
                    editable: true
                    wrap: true
                    from: 0
                    to: 60
                    stepSize: 1
                    Component.onCompleted: value = Math.floor(dc.timeStep)
                    onValueChanged: dc.timeStep = value + timestepsecs.value/60
                }
                Text {
                    anchors.verticalCenter: parent.verticalCenter
                    text: ":"
                }
                SpinBox {
                    id: timestepsecs
                    editable: true
                    wrap: true
                    from: 0
                    to: 55
                    stepSize: 5
                    Component.onCompleted: value = (Math.ceil(dc.timeStep) - dc.timeStep) * 60
                    onValueChanged: dc.timeStep = timestepmins.value + value/60
                }
            }

            Rectangle { // fill to keep settings compact towards the top
                Layout.fillHeight: true
                Layout.fillWidth: true
                color: "#00000000"
            }
        }
    }
}

/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
##^##*/
