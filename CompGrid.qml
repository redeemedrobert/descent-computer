import QtQuick

GridView {
    Component.onCompleted: function() {
        dc.calculateGrid()
    }
    interactive: false
    model: dc.gridContents
    cellHeight: cellH
    cellWidth: cellW
    delegate: Rectangle {
        width: cellW
        height: cellH
        border.color: "black"
        border.width: index % dc.columns == 0 || index <= dc.columns ? 2 : .5
        clip: true
        Text {
            anchors.centerIn: parent
            text: modelData
            font.bold: index % dc.columns == 0 || index <= dc.columns ? true : false
        }
    }
    Rectangle {
        id: gridlinexleft
        color: "#FF0000"
        width: 0
        height: 3
        x: cellW
        y: cellH + cellH / 2
        Behavior on y {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridlinexright
        color: "#FF0000"
        width: (dc.columns - 2) * cellW
        height: 3
        x: cellW * 2
        y: cellH + cellH / 2
        Behavior on y {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridlineytop
        color: "#FF0000"
        width: 3
        height: 0
        x: cellW + cellW / 2
        y: cellH
        Behavior on x {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridlineybottom
        color: "#FF0000"
        width: 3
        height: (dc.rows - 2) * cellH
        x: cellW + cellW / 2
        y: cellH * 2
        Behavior on x {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridcursorx
        color: "#00000000"
        border.width: 3
        border.color: "red"
        width: cellW
        height: cellH
        x: cellW
        Behavior on x {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridcursory
        color: "#00000000"
        border.width: 3
        border.color: "red"
        width: cellW
        height: cellH
        y: cellH
        Behavior on y {
            NumberAnimation {
                easing.type: Easing.OutCirc
            }
        }
    }
    Rectangle {
        id: gridcursor
        color: "#00000000"
        border.width: 3
        border.color: "red"
        width: cellW
        height: cellH
        x: cellW
        y: cellH
        MouseArea {
            property int lastMouseX: 0 // because Qt seems to be caching mouseX from last time press was true, see usage below
            id: cursorarea
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.OpenHandCursor
            onPositionChanged: (mouse) => {
                if(pressed) {
                    alignCursor()
               }
            }
            onReleased: {
                cursorarea.cursorReleased()
            }
            Connections {
                target: dc
                function onGridContentsChanged() {
                    cursorarea.alignCursor() // do not realign on flips
                    cursorarea.cursorReleased()
                }
            }

            function alignCursor() {
                var allowX = !dc.flipped || (cellW * 2 + gridlinexleft.width // still need to update X on window size changes
                    + gridlinexright.width) / cellW !== dc.columns           // when cursur + x line width doesn't match grid width
                if(allowX && pressed) lastMouseX = mouseX // prevents mouseX updates when grid is flipped
                var lineXPos = mouseY + gridcursor.y
                var lineYPos = allowX ? lastMouseX + gridcursor.x : gridcursor.x + cellW/2 // no movement when flipped
                if(lineXPos < cellH + cellH/2) lineXPos = cellH + cellH/2
                if(lineYPos < cellW + cellW/2) lineYPos = cellW + cellW/2
                if(lineXPos > cellH * dc.rows - cellH/2) lineXPos = cellH * dc.rows - cellH/2
                if(lineYPos > cellW * dc.columns - cellW/2) lineYPos = cellW * dc.columns - cellW/2
                gridlinexleft.y = lineXPos; gridlinexright.y = lineXPos
                gridlineytop.x = lineYPos; gridlineybottom.x = lineYPos
                gridcursorx.x = lineYPos - cellW/2
                gridcursory.y = lineXPos - cellH/2
                var gridPosX = allowX ? Math.floor((gridcursor.x + lastMouseX) / cellW) : Math.floor(gridcursor.x/cellW)
                var gridPosY = Math.floor((gridcursor.y + mouseY) / cellH)
                if(gridPosX < 1) gridPosX = 1
                if(gridPosY < 1) gridPosY = 1
                if(gridPosX >= dc.columns) gridPosX = dc.columns - 1
                if(gridPosY >= dc.rows) gridPosY = dc.rows - 1
                if(allowX) gridcursor.x = gridPosX * cellW
                gridcursor.y = gridPosY * cellH
                if(allowX) gridlinexleft.width = (gridPosX - 1) * cellW
                gridlineytop.height = (gridPosY - 1) * cellH
                if(allowX) gridlinexright.x = gridcursor.x + cellW
                gridlineybottom.y = gridcursor.y + cellH
                if(allowX) gridlinexright.width = (dc.columns - gridPosX - 1) * cellW
                gridlineybottom.height = (dc.rows - gridPosY - 1) * cellH
                if(allowX) // dont' set coords when table is flipped (unless grid columns amount changed)
                    dc.setCursor(gridPosX, gridPosY)
            }
            function cursorReleased() {
                gridlinexleft.y = gridcursor.y + cellH / 2; gridlinexright.y = gridcursor.y + cellH / 2
                gridlineytop.x = gridcursor.x + cellW / 2; gridlineybottom.x = gridcursor.x + cellW / 2
                gridcursorx.x = gridcursor.x
                gridcursory.y = gridcursor.y
            }
        }
    }
}
