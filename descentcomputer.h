#ifndef DESCENTCOMPUTER_H
#define DESCENTCOMPUTER_H

#include <QObject>
#include <QTimer>

class DescentComputer : public QObject
{
    Q_OBJECT

    Q_PROPERTY(QStringList gridContents READ gridContents NOTIFY gridContentsChanged)

    Q_PROPERTY(bool flipped         READ flipped WRITE setFlipped NOTIFY flippedChanged)

    Q_PROPERTY(qint16 rows          READ rows WRITE setRows NOTIFY rowsChanged)
    Q_PROPERTY(qint16 columns       READ columns WRITE setColumns NOTIFY columnsChanged)
    Q_PROPERTY(qint16 startSpeed    READ startSpeed WRITE setStartSpeed NOTIFY startSpeedChanged)
    Q_PROPERTY(qint16 speedStep     READ speedStep WRITE setSpeedStep NOTIFY speedStepChanged)
    Q_PROPERTY(qint16 startDist     READ startDist WRITE setStartDist NOTIFY startDistChanged)
    Q_PROPERTY(qint16 distStep      READ distStep WRITE setDistStep NOTIFY distStepChanged)
    Q_PROPERTY(qint32 startAlt      READ startAlt WRITE setStartAlt NOTIFY startAltChanged)
    Q_PROPERTY(qint16 altStep       READ altStep WRITE setAltStep NOTIFY altStepChanged)

    Q_PROPERTY(double startTime     READ startTime WRITE setStartTime NOTIFY startTimeChanged)
    Q_PROPERTY(double timeStep      READ timeStep WRITE setTimeStep NOTIFY timeStepChanged)

public:
    explicit DescentComputer(QObject *parent = nullptr);

    QStringList gridContents() const { return m_grid_contents; };

    QTimer* debounce;

    bool flipped() const          { return m_flipped; }

    qint16 rows() const         { return m_rows; }
    qint16 columns() const      { return m_columns; }
    qint16 startSpeed() const   { return m_start_speed; }
    qint16 speedStep() const    { return m_speed_step; }
    qint16 startDist() const    { return m_start_dist; }
    qint16 distStep() const     { return m_dist_step; }
    qint32 startAlt() const     { return m_start_alt; }
    qint16 altStep() const      { return m_alt_step; }

    double startTime() const    { return m_start_time; }
    double timeStep() const     { return m_time_step; }

    void setFlipped(const bool val)         { m_flipped = val; emit flippedChanged(); calculateGrid(); }

    void setRows(const qint16 val)          { m_rows = val; emit rowsChanged(); calculateGrid(); }
    void setColumns(const qint16 val)       { m_columns = val; emit columnsChanged(); calculateGrid(); }
    void setStartSpeed(const qint16 val)    { m_start_speed = val; emit startSpeedChanged(); calculateGrid(); }
    void setSpeedStep(const qint16 val)     { m_speed_step = val; emit speedStepChanged(); calculateGrid(); }
    void setStartDist(const qint16 val)     { m_start_dist = val; emit startDistChanged(); calculateGrid(); }
    void setDistStep(const qint16 val)      { m_dist_step = val; emit distStepChanged(); calculateGrid(); }
    void setStartAlt(const qint32 val)      { m_start_alt = val; emit startAltChanged(); calculateGrid(); }
    void setAltStep(const qint16 val)       { m_alt_step = val; emit altStepChanged(); calculateGrid(); }

    void setStartTime(const double val)     { m_start_time = val; emit startTimeChanged(); calculateGrid(); }
    void setTimeStep(const double val);     // defined in source

    Q_INVOKABLE void setCursor(qint16 x, qint16 y);

public slots:
    void calculateGrid();

private:

    void gridWork();

    QStringList m_grid_contents;

    bool m_flipped          { false };

    qint16 m_rows           { 11 };
    qint16 m_columns        { 11 };
    qint16 m_start_speed    { 500 };
    qint16 m_speed_step     { 50 };
    qint16 m_start_dist     { 50 };
    qint16 m_dist_step      { 10 };
    qint32 m_start_alt      { 30000 };
    qint16 m_alt_step       { 5000 };

    double m_start_time     { 10.0 };
    double m_time_step      { 5.0 };

    qint16 m_cursor_x       { 1 };
    qint16 m_cursor_y       { 1 };

signals:
    void gridContentsChanged();

    void flippedChanged();

    void rowsChanged();
    void columnsChanged();
    void startSpeedChanged();
    void speedStepChanged();
    void startDistChanged();
    void distStepChanged();
    void startAltChanged();
    void altStepChanged();

    void startTimeChanged();
    void timeStepChanged();
};

#endif // DESCENTCOMPUTER_H
