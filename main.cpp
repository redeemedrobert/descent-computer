#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "descentcomputer.h"


int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    DescentComputer dc;

    QQmlApplicationEngine engine;
    const QUrl url(u"qrc:/descent-computer/main.qml"_qs);
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);

    engine.rootContext()->setContextProperty("dc", &dc);
    engine.load(url);

    return app.exec();
}
