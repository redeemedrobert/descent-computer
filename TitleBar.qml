import QtQuick
import QtQuick.Layouts

Rectangle {
    id: titlebar
    Layout.fillWidth: true
    height: 25
    color: "#000000"
    z: 1

    Rectangle {
        id: title
        anchors.centerIn: parent
        height: 20
        width: 300
        color: "darkgray"
        Rectangle {
            id: rplus
            height: 20
            width: 20
            anchors.right: title.left
            color: "gray"
            visible: Qt.platform.os === "windows" || Qt.platform.os === "osx" || Qt.platform.os === "linux"
            Text {
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                text: "+"
                color: "white"
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    window.height += cellH
                }
                onEntered: rplus.color = "darkgray"
                onExited: rplus.color = "gray"
            }
        }
        Rectangle {
            id: rminus
            height: 20
            width: 20
            anchors.right: rplus.left
            color: "gray"
            visible: Qt.platform.os === "windows" || Qt.platform.os === "osx" || Qt.platform.os === "linux"
            Text {
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                text: "-"
                color: "white"
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    window.height -= cellH
                }
                onEntered: rminus.color = "darkgray"
                onExited: rminus.color = "gray"
            }
        }
        Rectangle {
            id: cminus
            height: 20
            width: 20
            anchors.left: title.right
            color: "gray"
            visible: Qt.platform.os === "windows" || Qt.platform.os === "osx" || Qt.platform.os === "linux"
            Text {
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                text: "-"
                color: "white"
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    window.width -= cellW
                }
                onEntered: cminus.color = "darkgray"
                onExited: cminus.color = "gray"
            }
        }
        Rectangle {
            id: cplus
            height: 20
            width: 20
            anchors.left: cminus.right
            color: "gray"
            visible: Qt.platform.os === "windows" || Qt.platform.os === "osx" || Qt.platform.os === "linux"
            Text {
                anchors.fill: parent
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                text: "+"
                color: "white"
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                hoverEnabled: true
                onClicked: {
                    window.width += cellW
                }
                onEntered: cplus.color = "darkgray"
                onExited: cplus.color = "gray"
            }
        }
        Text {
            anchors.centerIn: parent
            text: dc.columns > 4 ? "DC1 Descent Computer" : "DC1"
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        MouseArea {
            property int offsetX
            property int offsetY
            anchors.fill: parent
            hoverEnabled: true
            cursorShape: Qt.SizeAllCursor
            onPressed: {
                offsetX = (x + width / 2) - mouseX
                offsetY = (y + height / 2) - mouseY
            }

            onPositionChanged: (mouse) => {
                if(pressed) {
                   window.x += mouseX - (x + width / 2) + offsetX
                   window.y += mouseY - (y + height / 2) + offsetY
               }
            }
        }
    }

    Rectangle {
        id: flipbutton
        height: 25
        width: 25
        anchors.right: settingsbutton.left
        color: "gray"
        Text {
            anchors.fill: parent
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            text: "↻"
            color: "white"
            font.bold: true
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: {
                contentgrid.opacity = 0
                dc.flipped = !dc.flipped
            }
            onEntered: flipbutton.color = "darkgray"
            onExited: flipbutton.color = "gray"
        }
    }

    Rectangle {
        id: settingsbutton
        height: 25
        width: 25
        anchors.right: parent.right
        color: "gray"
        Text {
            anchors.fill: parent
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            text: "⚙"
            color: "white"
            font.bold: true
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: settingspanel.opacity = settingspanel.visible ? 0 : 1
            onEntered: settingsbutton.color = "darkgray"
            onExited: settingsbutton.color = "gray"
        }
    }

    Rectangle {
        id: closebutton
        height: 25
        width: 25
        anchors.left: parent.left
        color: "red"
        Text {
            anchors.fill: parent
            horizontalAlignment: Qt.AlignHCenter
            verticalAlignment: Qt.AlignVCenter
            text: "╳"
            color: "white"
            font.bold: true
        }
        MouseArea {
            anchors.fill: parent
            hoverEnabled: true
            onClicked: function() { Qt.quit() }
            onEntered: closebutton.color = "darkred"
            onExited: closebutton.color = "red"
        }
    }
}
